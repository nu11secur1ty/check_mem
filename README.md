# check_mem - nagios check memory
![check_mem](https://raw.githubusercontent.com/nu11secur1ty/check_mem/master/Nagios_memory.png)
```
/check_mem.pl -f -C -w 10 -c 5
```
## define command in nrpe.cfg:
   command[check_mem]=/usr/lib64/nagios/plugins/check_mem.pl -f -C -w 10 -c 5
